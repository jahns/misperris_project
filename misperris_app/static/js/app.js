//cargar info
$(function(){
    cargarRegion()
    cargarViviendas()
    cargarEstados()
    cargarPreguntas()
})

function cargarRegion(){
    chile.regiones.forEach(region => {
        $("#region").append('<option value="'+region.nombre+'">'+region.nombre+'</option>')
    })
}

$("#region").change(function(){
    $("#comuna").html("");
    $("#comuna").append('<option hidden value="default">Seleccione Comuna</option>')
    var region = $(this).val()
    var regionComunas = chile.regiones.find(r => r.nombre == region)
    regionComunas.comunas.forEach(comuna => {
        $("#comuna").append('<option value="'+comuna+'">'+comuna+'</option>')
    })
})

var chile = {
    "regiones": [
        {
            "nombre": "Arica y Parinacota",
            "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
        },
        {
            "nombre": "Tarapacá",
            "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
        },
        {
            "nombre": "Antofagasta",
            "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
        },
        {
            "nombre": "Atacama",
            "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
        },
        {
            "nombre": "Coquimbo",
            "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
        },
        {
            "nombre": "Valparaíso",
            "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
        },
        {
            "nombre": "Región del Libertador Gral. Bernardo O’Higgins",
            "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
        },
        {
            "nombre": "Región del Maule",
            "comunas": ["Talca", "Constítución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "Retíro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
        },
        {
            "nombre": "Región del Biobío",
            "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
        },
        {
            "nombre": "Región de la Araucanía",
            "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria", ]
        },
        {
            "nombre": "Región de Los Ríos",
            "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
        },
        {
            "nombre": "Región de Los Lagos",
            "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "Frutíllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
        },
        {
            "nombre": "Región Aisén del Gral. Carlos Ibáñez del Campo",
            "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
        },
        {
            "nombre": "Región de Magallanes y de la Antártíca Chilena",
            "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártíca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
        },
        {
            "nombre": "Región Metropolitana de Santiago",
            "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltíl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
    }]
}

function cargarViviendas(){
    viviendas.forEach(vivienda => {
        $("#vivienda").append('<option value="'+vivienda+'">'+vivienda+'</option>')
    })
}

var viviendas = ["Casa con patio grande", "Casa con patio pequeño", "Casa sin patio", "Departamento"]

function cargarEstados(){
    estados.forEach(estado => {
        $("#estado").append('<option value="'+estado+'">'+estado+'</option>')
    })
}

var estados = ["Rescatado", "Disponible", "Adoptado"]

function cargarPreguntas(){
    preguntas.forEach(pregunta => {
        $("#pregunta").append('<option value="'+pregunta+'">'+pregunta+'</option>')
    })
}

var preguntas = ['¿Cuál es el nombre de mi madre?', '¿Cuál fue mi primera mascota?', '¿Cuál es mi serie de televisión favorita?', '¿Cuál es mi color favorito?']

//efecto slide
$(window).scroll(function(){
    $(".slideanim").each(function(){
        var pos = $(this).offset().top;
        var winTop = $(window).scrollTop();
        if (pos < winTop + 600){
        $(this).addClass("slidescroll");
        }
    });
});

//uso general
//uso general - ajustar dropdown menu
$(window).resize(function(){
    if ($("#perris-navbar").width() <= 990){
        $("#perris-navbar").attr('class', 'navbar navbar-dark bg-primary navbar-style-pwa justify-content-center')
        $("#perris-navbar-dropdown").attr('class', 'dropdown-menu dropdown-user-style')
    } else {
        $("#perris-navbar").attr('class', 'navbar navbar-dark bg-primary navbar-style-pwa')
        $("#perris-navbar-dropdown").attr('class', 'dropdown-menu dropdown-menu-right dropdown-user-style')
    }
})

//uso general - pwa links
function pwa(){
    $("#pwa").html('')
    $("#pwa").load('/index_pwa/')
}

$("#index_pwa").on('click', function(){
    $("#pwa").html('')
    $("#pwa").load('/index_pwa/')
})

$("#registro_pwa").on('click', function(){
    $("#pwa").html('')
    $("#pwa").load('/registro_pwa/')
})

$("#rescatados_pwa").on('click', function(){
    $("#pwa").html('')
    $("#pwa").load('/rescatados_pwa/')
    x = $("#userAuth").val()

    if (x){
        rescatados()
    } else{
        rescatados_nolog()
    }
})

$("#galeria_pwa").on('click', function(){
    $("#pwa").html('')
    $("#pwa").load('/galeria_pwa/')

    cargar_galerias()
})

//acciones misperris
//acciones misperris - habilitar csrf_token ajax
function getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != ''){
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++){
            var cookie = jQuery.trim(cookies[i]);

            if (cookie.substring(0, name.length + 1) == (name + '=')){
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({ 
    beforeSend: function(xhr, settings){
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))){
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    } 
});

//acciones misperris - timer de alerts
function timerAlert(){
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove()
        })
    }, 5000)
}

//acciones misperris - agregar usuario
function agregar_usuario(){
    //obtener datos
    run = $("#run").val()
    nombre = $("#nombre").val()
    nacimiento = $("#nacimiento").val()
    telefono = $("#telefono").val()
    if (telefono == ""){
        telefono = 0
    }
    correo = $("#correo").val()
    username = $("#username").val()
    password = $("#password").val()
    pregunta = $("#pregunta").val()
    respuesta = $("#respuesta").val()
    inputAvatar = document.getElementById("avatar")
    avatar = inputAvatar.files[0]
    region = $("#region").val()
    comuna = $("#comuna").val()
    vivienda = $("#vivienda").val()
    provider = "misperris"

    //crear form data
    data = new FormData()
    data.append('run', run)
    data.append('nombre', nombre)
    data.append('nacimiento', nacimiento)
    data.append('telefono', telefono)
    data.append('correo', correo)
    data.append('username', username)
    data.append('password', password)
    data.append('pregunta', pregunta)
    data.append('respuesta', respuesta)
    data.append('avatar', avatar)
    data.append('region', region)
    data.append('comuna', comuna)
    data.append('vivienda', vivienda)
    data.append('provider', provider)

    //enviar con ajax
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: '/usuario/agregar_usuario',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(message){
            html = ''
            html += '<div class="alert alert-success alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            html += message
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            html += '<span aria-hidden="true">&times;</span>'
            html += '</button>'
            html += '</div>'
            $("#message-user").html(html)
            $("#reset-user").click()
            timerAlert()
        },
        error: function(e){
            console.log(e)
            html = ''
            html += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            html += 'Registro incorrecto'
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            html += '<span aria-hidden="true">&times;</span>'
            html += '</button>'
            html += '</div>'
            $("#message-user").html(html)
            $("#reset-user").click()
            timerAlert()
        }
    })
}

//acciones misperris - obtener usuario social
function social(){
    s = "'" //separador

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: null,
        url: 'http://localhost:8000/misperrisApi/social/',
        type: 'get',
        success: function(data){
            data.forEach(dato => {
                //convertir extra data a string
                function extraerTexto(string){
                    regex = /'(.*?)'/g;
                    result = [];
                    var current;

                    while (current = regex.exec(string)) {
                        result.push(current.pop());
                    }

                    return result.length > 0
                        ? result
                        : [string];
                }

                social_data = extraerTexto(dato.extra_data)
                
                //convertir extra data a json
                var row = []
                var row_data = []

                //extraer datos
                for (var i = 0; i < social_data.length; i++) {
                    //filas
                    if (i % 2 == 0) {
                        row.push(social_data[i])
                    }

                    //datos
                    if (i % 2 == 1) {
                        row_data.push(social_data[i])
                    }            
                }

                //convertir a json
                var arrayJson = []
                var social_json = new Object()
                for (var i = 0; i < row.length; i++) {
                    social_json[row[i]] = row_data[i]
                }
                arrayJson.push(social_json)

                //recorrer social json y enviar info
                socialUser = $("#socialUser").val()
                email = $("#socialEmail").val()

                arrayJson.forEach(social => {
                    if (email == social.email) {
                        cargar_usuario_social(social.name, social.email, socialUser)
                    }
                })
            })
        }
    })
}

//acciones misperris - cargar info usuario social
function cargar_usuario_social(nombre, correo, username){
    //deshabilitar
    $("#nombre").prop('disabled', true)
    $("#correo").prop('disabled', true)
    $("#username").prop('disabled', true)
    $("#password").prop('disabled', true)
    $("#repassword").prop('disabled', true)
    $("#pregunta").prop('disabled', true)
    $("#respuesta").prop('disabled', true)
    $("#reset-user").prop('disabled', true)

    //enviar info
    provider = "provided-by-facebook"
    $("#nombre").val(nombre)
    $("#correo").val(correo)
    $("#username").val(username)
    $("#password").val(provider)
    $("#repassword").val(provider)
    $("#pregunta").append('<option hidden value="provided" selected>'+provider+'</option>')
    $("#respuesta").val(provider)
}

//acciones misperris - agregar usuario social
function agregar_usuario_social(){
    //obtener datos
    run = $("#run").val()
    nombre = $("#nombre").val()
    nacimiento = $("#nacimiento").val()
    telefono = $("#telefono").val()
    if (telefono == ""){
        telefono = 0
    }
    correo = $("#correo").val()
    username = $("#username").val()
    password = $("#password").val()
    pregunta = $("#pregunta").val()
    respuesta = $("#respuesta").val()
    inputAvatar = document.getElementById("avatar")
    avatar = inputAvatar.files[0]
    region = $("#region").val()
    comuna = $("#comuna").val()
    vivienda = $("#vivienda").val()
    provider = "facebook"

    //crear form data
    data = new FormData()
    data.append('run', run)
    data.append('nombre', nombre)
    data.append('nacimiento', nacimiento)
    data.append('telefono', telefono)
    data.append('correo', correo)
    data.append('usuario', username)
    data.append('password', password)
    data.append('pregunta', pregunta)
    data.append('respuesta', respuesta)
    data.append('avatar', avatar)
    data.append('region', region)
    data.append('comuna', comuna)
    data.append('vivienda', vivienda)
    data.append('provider', provider)

    //enviar con ajax
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: 'http://localhost:8000/misperrisApi/usuarios/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(){
            html = ''
            html += '<div class="alert alert-success alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            html += 'Registro correcto'
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            html += '<span aria-hidden="true">&times;</span>'
            html += '</button>'
            html += '</div>'
            $("#message-user").html(html)
            $("#reset-user").click()
            
            window.setTimeout(function() {
                window.location.href = "/"
            }, 1000)
        },
        error: function(e){
            console.log(e)
            html = ''
            html += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            html += 'Registro incorrecto'
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            html += '<span aria-hidden="true">&times;</span>'
            html += '</button>'
            html += '</div>'
            $("#message-user").html(html)
            $("#reset-user").click()
            timerAlert()
        }
    })
}

//acciones misperris - accion formulario rescued
function action_rescued(){
    action = $("#action").val()

    if (action == 'post') {
        agregar_rescued()
    } else if(action == 'put'){
        editar_rescued()
    } else{
        message = ''
        message += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
        message += '<b>Acción incorrecta</b>, por favor recargue la página'
        message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        message += '<span aria-hidden="true">&times;</span>'
        message += '</button>'
        message += '</div>'
        $("#message-rescued").html(message)
        timerAlert()
    }
}

//acciones misperris - listar rescatados
function rescatados(){
    s = "'" //separador

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: null,
        url: 'http://localhost:8000/misperrisApi/rescatados/',
        type: 'get',
        success: function(data){
            data.forEach(dato => {
                //listar rescatados
                html = ''
                html += '<div class="row row-rescued-style slidescroll slideanim">'
                html += '<div class="col-md-1 col-rescued-style">'
                html += dato.id
                html += '</div>'
                html += '<div class="col-md-2 col-rescued-style">'
                html += '<img src="'+dato.foto+'" alt="error-res-foto" class="rescued-img">'
                html += '</div>'
                html += '<div class="col-md-2 col-rescued-style">'
                html += dato.nombre
                html += '</div>'
                html += '<div class="col-md-2 col-rescued-style">'
                html += dato.raza
                html += '</div>'
                html += '<div class="col-md-2 col-rescued-style">'
                html += dato.descripcion
                html += '</div>'
                html += '<div class="col-md-1 col-rescued-style">'
                html += dato.estado
                html += '</div>'
                html += '<div class="col-md-1 col-rescued-style">'
                //editar
                html += '<a href="#editar" onclick="editar_data_rescued('+s+dato.url+s+', '+s+dato.id+s+', '+s+dato.nombre+s+', '+s+dato.raza+s+', '+s+dato.descripcion+s+', '+s+dato.estado+s+')">Editar</a>'
                //fin editar
                html += '</div>'
                html += '<div class="col-md-1 col-rescued-style">'
                //eliminar
                html += '<a href="#" data-toggle="modal" data-target="#delete'+dato.id+'">Eliminar</a>'
                //fin eliminar
                html += '</div>'
                html += '</div>'
                $("#rescatados").append(html)

                //rescatados delete
                del = ''
                del += '<div class="modal fade modal-container" id="delete'+dato.id+'">'
                del += '<div class="modal-dialog modal-dialog-centered">'
                del += '<div class="modal-content perris-modal-container">'
                del += '<div class="modal-header perris-modal-header">'
                del += '<h4 class="modal-title">Advertencia</h4>'
                del += '<button type="button" class="close" data-dismiss="modal">&times;</button>'
                del += '</div>'
                del += '<div class="modal-body perris-modal-body">'
                del += '<div class="container">'
                del += '<span class="delete-text">¿Está seguro que desea eliminar al rescatado "'+dato.nombre+'"?</span>'
                del += '</div>'
                del += '</div>'
                del += '<div class="modal-footer perris-modal-footer">'
                del += '<div class="container">'
                //form eliminar
                del += '<form action="#" data-method="DELETE">'
                del += '<input type="hidden" id="url'+dato.id+'" value="'+dato.url+'">'
                del += '<div class="row">'
                del += '<div class="col-6 col-md-6">'
                del += '<button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cerrar</button>'
                del += '</div>'
                del += '<div class="col-6 col-md-6">'
                del += '<button type="button" class="btn btn-success btn-block" data-dismiss="modal" onclick="eliminar_rescued('+s+dato.id+s+')">Eliminar</button>'
                del += '</div>'
                del += '</div>'
                del += '</form>'
                //fin form eliminar
                del += '</div>'
                del += '</div>'
                del += '</div>'
                del += '</div>'
                del += '</div>'
                $("#rescatados-delete").append(del)
            })
        }
    })
}

//acciones misperris - listar rescatados no log
function rescatados_nolog(){
    s = "'" //separador

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: null,
        url: 'http://localhost:8000/misperrisApi/rescatados/',
        type: 'get',
        success: function(data){
            data.forEach(dato => {
                if (dato.estado == "Disponible") {
                    //listar rescatados
                    html = ''
                    html += '<div class="row row-rescued-style slidescroll slideanim">'
                    html += '<div class="col-md-1 col-rescued-style">'
                    html += dato.id
                    html += '</div>'
                    html += '<div class="col-md-2 col-rescued-style">'
                    html += '<img src="'+dato.foto+'" alt="error-res-foto" class="rescued-img">'
                    html += '</div>'
                    html += '<div class="col-md-2 col-rescued-style">'
                    html += dato.nombre
                    html += '</div>'
                    html += '<div class="col-md-2 col-rescued-style">'
                    html += dato.raza
                    html += '</div>'
                    html += '<div class="col-md-3 col-rescued-style">'
                    html += dato.descripcion
                    html += '</div>'
                    html += '<div class="col-md-2 col-rescued-style">'
                    html += dato.estado
                    html += '</div>'
                    html += '</div>'
                    $("#rescatados").append(html)
                }
            })
        }
    })
}

//acciones misperris - resetear formulario rescued
function reset_rescued(){
    $("#url").val("")
    $("#id").val("")
    $("#nombrerescued").val("")
    $("#raza").val("")
    $("#foto").val("")
    $("#descripcion").val("")
    $("#estado").val("default")
}

//acciones misperris - agregar rescatado
function agregar_rescued(){
    //obtener
    nombre = $("#nombrerescued").val()
    raza = $("#raza").val()
    inputFoto = document.getElementById("foto")
    foto = inputFoto.files[0]
    descripcion = $("#descripcion").val()
    estado = $("#estado").val()

    //crear form data
    data = new FormData()
    data.append('nombre', nombre)
    data.append('raza', raza)
    data.append('foto', foto)
    data.append('descripcion', descripcion)
    data.append('estado', estado)
    
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: 'http://localhost:8000/misperrisApi/rescatados/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(e){
            //actualizar tabla
            html = ''
            $("#rescatados").html(html)
            del = ''
            $("#rescatados-delete").append(del)
            rescatados()

            //enviar mensaje
            message = ''
            message += '<div class="alert alert-success alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Registro correcto'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-rescued").html(message)
            reset_rescued()
            timerAlert()
        },
        error: function(e){
            message = ''
            message += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Registro incorrecto'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-rescued").html(message)
            reset_rescued()
            timerAlert()
        }
    })
}

//acciones misperris - click editar rescatado
function editar_data_rescued(url, id, nombre, raza, descripcion, estado){
    $("#action").val('put')
    $("#url").val(url)
    $("#id").val(id)
    $("#nombrerescued").val(nombre)
    $("#raza").val(raza)
    $("#descripcion").val(descripcion)
    $("#estado").val(estado)
    $("#title-rescued").text('Mantenedor de rescatados (Editar rescatado "'+$("#nombrerescued").val()+'")')
    $("#buttons-rescued").attr('class', 'row d-none')
    $("#buttons-editar-rescued").attr('class', 'row')
}

//acciones misperris - editar rescatado
function editar_rescued(){
    //obtener
    url = $("#url").val()
    nombre = $("#nombrerescued").val()
    raza = $("#raza").val()
    inputFoto = document.getElementById("foto")
    foto = inputFoto.files[0]
    descripcion = $("#descripcion").val()
    estado = $("#estado").val()

    //crear form data
    data = new FormData()
    data.append('nombre', nombre)
    data.append('raza', raza)
    data.append('foto', foto)
    data.append('descripcion', descripcion)
    data.append('estado', estado)
    
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: url,
        type: 'PUT',
        contentType: false,
        processData: false,
        cache: false,
        success: function(e){
            //actualizar tabla
            html = ''
            $("#rescatados").html(html)
            del = ''
            $("#rescatados-delete").append(del)
            rescatados()

            //enviar mensaje
            message = ''
            message += '<div class="alert alert-success alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Rescatado editado'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-rescued").html(message)
            reset_rescued()
            cancelar_editar_rescatado()
            timerAlert()
        },
        error: function(e){
            message = ''
            message += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Rescatado no editado'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-rescued").html(message)
            reset_rescued()
            timerAlert()
        }
    })
}

//acciones misperris - cancelar editar rescatado
function cancelar_editar_rescatado(){
    reset_rescued()
    $("#action").val('post')
    $("#title-rescued").text('Mantenedor de rescatados')
    $("#buttons-rescued").attr('class', 'row')
    $("#buttons-editar-rescued").attr('class', 'row d-none')
}

//acciones misperris - eliminar rescatado
function eliminar_rescued(id){
    //obtener url
    url = $("#url"+id).val()

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: null,
        url: url,
        type: 'DELETE',
        success: function(e){
            //actualizar tabla
            html = ''
            $("#rescatados").html(html)
            del = ''
            $("#rescatados-delete").append(del)
            rescatados()

            //enviar mensaje
            message = ''
            message += '<div class="alert alert-success alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Rescatado eliminado'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-delete-rescued").html(message)
            timerAlert()
        },
        error: function(e){
            //enviar mensaje
            message = ''
            message += '<div class="alert alert-danger alert-dismissible fade show text-center slidescroll slideanim" role="alert">'
            message += 'Rescatado no eliminado'
            message += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            message += '<span aria-hidden="true">&times;</span>'
            message += '</button>'
            message += '</div>'
            $("#message-delete-rescued").html(message)
            timerAlert()
        }
    })
}

//acciones misperris - galeria
function cargar_galerias(){
    s = "'" //separador

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: null,
        url: 'http://localhost:8000/misperrisApi/rescatados/',
        type: 'get',
        success: function(data){
            data.forEach(dato => {
                //cargar modales
                modal = ''
                modal += '<!-- Modal galery -->'
                modal += '<div class="modal fade modal-container" id="'+dato.nombre+'">'
                modal += '<div class="modal-dialog modal-dialog-centered">'
                modal += '<div class="modal-content perris-modal-container">'
                modal += '<!-- Modal header -->'
                modal += '<div class="modal-header perris-modal-header">'
                modal += '<h4 class="modal-title">'+dato.nombre+'</h4>'
                modal += '<button type="button" class="close" data-dismiss="modal">&times;</button>'
                modal += '</div>'
                modal += '<!-- Modal body -->'
                modal += '<div class="modal-body perris-modal-galery-body" style="background-image: url('+s+dato.foto+s+')"></div>'
                modal += '<div class="modal-body perris-modal-description">'
                modal += '<div class="row">'
                modal += '<div class="col-6 col-md-6">'
                modal += '<span>Raza predominante:</span>'
                modal += '<span>Descripción:</span>'
                modal += '<span>Estado:</span>'
                modal += '</div>'
                modal += '<div class="col-6 col-md-6">'
                modal += '<span>'+dato.raza+'</span>'
                modal += '<span>'+dato.descripcion+'</span>'
                modal += '<span>'+dato.estado+'</span>'
                modal += '</div>'
                modal += '</div>'
                modal += '</div>'
                modal += '<!-- Modal footer -->'
                modal += '<div class="modal-footer perris-modal-footer">'
                modal += '<button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cerrar</button>'
                modal += '</div>'
                modal += '</div>'
                modal += '</div>'
                modal += '</div>'
                $("#galeriaModal").append(modal)

                //cargar galerias
                if (dato.estado == "Rescatado") {
                    html = ''
                    html += '<div class="col-md-3">'
                    html += '<a href="#" data-toggle="modal" data-target="#'+dato.nombre+'">'
                    html += '<img src="'+dato.foto+'" alt="error-galeria" class="galery-img slidescroll slideanim">'
                    html += '</a>'
                    html += '</div>'
                    $("#galeriaRescatados").append(html)
                } else if (dato.estado == "Disponible"){
                    html = ''
                    html += '<div class="col-md-3">'
                    html += '<a href="#" data-toggle="modal" data-target="#'+dato.nombre+'">'
                    html += '<img src="'+dato.foto+'" alt="error-galeria" class="galery-img slidescroll slideanim">'
                    html += '</a>'
                    html += '</div>'
                    $("#galeriaDisponibles").append(html)
                } else if (dato.estado == "Adoptado"){
                    html = ''
                    html += '<div class="col-md-3">'
                    html += '<a href="#" data-toggle="modal" data-target="#'+dato.nombre+'">'
                    html += '<img src="'+dato.foto+'" alt="error-galeria" class="galery-img slidescroll slideanim">'
                    html += '</a>'
                    html += '</div>'
                    $("#galeriaAdoptados").append(html)
                }
            })
        }
    })
}

//validaciones
//validaciones - validar letras
function validarLetras(e){
    key = e.keyCode || e.wich;
    tec = String.fromCharCode(key).toLocaleLowerCase();
    letras = "abcdefghijklmnñopqrstuvwxyzáéíóú ";
    especial = "8-37-38-46-164";
    tec_especial = false;

    for (var i in especial){
        if(key == especial[i]){
            tec_especial = true;
            break;
        }
    }

    if (letras.indexOf(tec) == -1 && !tec_especial){
        return false;
    }
}

//validaciones - validar run
function validarRun(e){
    key = e.keyCode || e.wich;
    tec = String.fromCharCode(key).toLocaleLowerCase();
    letras = "1234567890-k";
    especial = "8-37-38-46-164";
    tec_especial = false;

    for (var i in especial){
        if(key == especial[i]){
            tec_especial = true;
            break;
        }
    }

    if (letras.indexOf(tec) == -1 && !tec_especial){
        return false;
    }
}

//validaciones - fecha
function validarFecha(fecha){
	var dtCh= "/";
	var minYear=1900;
	var maxYear=2100;
	function isInteger(s){
		var i;
		for (i = 0; i < s.length; i++){
			var c = s.charAt(i);
			if (((c < "0") || (c > "9"))) return false;
		}
		return true;
	}
	function stripCharsInBag(s, bag){
		var i;
		var returnString = "";
		for (i = 0; i < s.length; i++){
			var c = s.charAt(i);
			if (bag.indexOf(c) == -1) returnString += c;
		}
		return returnString;
	}
	function daysInFebruary (year){
		return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i == 4 || i == 6 || i == 9 || i == 11) {this[i] = 30}
			if (i == 2) {this[i] = 29}
		}
		return this
	}
	function isDate(dtStr){
		var daysInMonth = DaysArray(12)
		var pos1 = dtStr.indexOf(dtCh)
		var pos2 = dtStr.indexOf(dtCh,pos1+1)
		var strDay = dtStr.substring(0,pos1)
		var strMonth = dtStr.substring(pos1+1,pos2)
		var strYear = dtStr.substring(pos2+1)
		strYr = strYear
		if (strDay.charAt(0) == "0" && strDay.length>1) strDay = strDay.substring(1)
		if (strMonth.charAt(0) == "0" && strMonth.length>1) strMonth = strMonth.substring(1)
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0) == "0" && strYr.length>1) strYr=strYr.substring(1)
		}
		month = parseInt(strMonth)
		day = parseInt(strDay)
		year = parseInt(strYr)
		if (pos1 == -1 || pos2 == -1){
			return false
		}
		if (strMonth.length<1 || month<1 || month>12){
			return false
		}
		if (strDay.length<1 || day<1 || day>31 || (month == 2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
			return false
		}
		if (strYear.length != 4 || year == 0 || year<minYear || year>maxYear){
			return false
		}
		if (dtStr.indexOf(dtCh,pos2+1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false){
			return false
		}
		return true
	}
	if(isDate(fecha)){
		return true;
	}else{
		return false;
	}
}

//validaciones - formato run
function validarFormatoRun(run){
    if (/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(run)){
        return true
    } else {
        return false
    }
}

//validaciones - crear metodo para validar selects jquery
$.validator.addMethod("requiredSelect", function(value, element, arg){
    return arg !== value
})

$.validator.addMethod("runFormat", function(value){
    format = false
    if (value){
        run = document.getElementById("run").value
        if ((validarFormatoRun(run))){
            format = true
        }
    }
    return format
}, "Ingrese un Run valido (ej: 11111111-1)")

//validaciones - crear metodo para validar fecha jquery
$.validator.addMethod("dateFormat", function(value){
    format = false
    if (value){
        fecha = document.getElementById("nacimiento").value
        if((validarFecha(fecha) && fecha.substr(6,10) < 2001)) {
            format = true
        }
    }
    return format
}, "Ingrese una fecha valida (ej: 01/01/1991)")

//formulario login
$("#formulary-login").validate({
    rules: {
        username: {
            required: true,
            maxlength: 40
        },
        passwordlogin: {
            required: true,
            maxlength: 50
        },
    },
    messages: {
        username: {
            required: "Debe ingresar un usuario",
            maxlength: "Largo maximo 40"
        },
        passwordlogin: {
            required: "Debe ingresar una contraseña",
            maxlength: "Largo maximo 50"
        },
    }
})

//formulario password
$("#formulary-password").validate({
    rules: {
        respuesta: {
            required: true,
            maxlength: 60
        },
        newpassword: {
            required: true,
            maxlength: 50,
            equalTo: "#newrepassword"
        },
        newrepassword: {
            required: true,
            maxlength: 50,
            equalTo: "#newpassword"
        },
        username: {
            required: true,
            maxlength: 40
        }
    },
    messages: {
        respuesta: {
            required: "Debe ingresar una respuesta",
            maxlength: "Largo maximo 60"
        },
        newpassword: {
            required: "Debe ingresar una contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        newrepassword: {
            required: "Debe confirmar su contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        username: {
            required: "Debe ingresar un usuario",
            maxlength: "Largo maximo 40"
        }
    }
})

//formulario password_nolog
$("#formulary-password_nolog").validate({
    rules: {
        respuesta: {
            required: true,
            maxlength: 60
        },
        newpassword: {
            required: true,
            maxlength: 50,
            equalTo: "#newrepassword"
        },
        newrepassword: {
            required: true,
            maxlength: 50,
            equalTo: "#newpassword"
        }
    },
    messages: {
        respuesta: {
            required: "Debe ingresar una respuesta",
            maxlength: "Largo maximo 60"
        },
        newpassword: {
            required: "Debe ingresar una contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        newrepassword: {
            required: "Debe confirmar su contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        }
    }
})

//formulario de usuarios
$("#formulary").validate({
    rules: {
        correo: {
            required: true,
            email: true,
            maxlength: 60
        },
        run: {
            required: true,
            runFormat: true,
            maxlength: 10
        },
        nombre: {
            required: true,
            maxlength: 100
        },
        nacimiento: {
            required: true,
            dateFormat: true,
            maxlength: 10
        },
        telefono: {
            number: true,
            maxlength: 9
        },
        username: {
            required: true,
            maxlength: 40
        },
        password: {
            required: true,
            maxlength: 50,
            equalTo: "#repassword"
        },
        repassword: {
            required: true,
            maxlength: 50,
            equalTo: "#password"
        },
        pregunta: {
            requiredSelect: "default"
        },
        respuesta: {
            required: true,
            maxlength: 60
        },
        avatar: {
            required: true
        },
        region: {
            requiredSelect: "default"
        },
        comuna: {
            requiredSelect: "default"
        },
        vivienda: {
            requiredSelect: "default"
        },
    },
    messages: {
        correo: {
            required: "Debe ingresar un correo",
            email: "Ingrese un correo valido (ej: user@domain.ex)",
            maxlength: "Largo maximo 60"
        },
        run: {
            required: "Debe ingresar un Run",
            maxlength: "Largo maximo 10"
        },
        nombre: {
            required: "Debe ingresar un nombre",
            maxlength: "Largo maximo 100"
        },
        nacimiento: {
            required: "Debe ingresar una fecha de nacimiento",
            maxlength: "Largo maximo 10"
        },
        telefono: {
            number: "Debe ingresar un telefono valido",
            maxlength: "Largo maximo 9"
        },
        username: {
            required: "Debe ingresar un usuario",
            maxlength: "Largo maximo 40"
        },
        password: {
            required: "Debe ingresar una contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        repassword: {
            required: "Debe confirmar su contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        pregunta: {
            requiredSelect: "Seleccionar una pregunta"
        },
        respuesta: {
            required: "Debe ingresar una respuesta",
            maxlength: "Largo maximo 60"
        },
        avatar: {
            required: "Debe seleccionar un avatar"
        },
        region: {
            requiredSelect: "Seleccione una region"
        },
        comuna: {
            requiredSelect: "Seleccione una comuna"
        },
        vivienda: {
            requiredSelect: "Seleccione una vivienda"
        }
    },
    submitHandler: function(form){
        agregar_usuario()
    }
})

$("#formulary-social").validate({
    rules: {
        correo: {
            required: true,
            email: true,
            maxlength: 60
        },
        run: {
            required: true,
            runFormat: true,
            maxlength: 10
        },
        nombre: {
            required: true,
            maxlength: 100
        },
        nacimiento: {
            required: true,
            dateFormat: true,
            maxlength: 10
        },
        telefono: {
            number: true,
            maxlength: 9
        },
        username: {
            required: true,
            maxlength: 40
        },
        password: {
            required: true,
            maxlength: 50,
            equalTo: "#repassword"
        },
        repassword: {
            required: true,
            maxlength: 50,
            equalTo: "#password"
        },
        pregunta: {
            requiredSelect: "default"
        },
        respuesta: {
            required: true,
            maxlength: 60
        },
        avatar: {
            required: true
        },
        region: {
            requiredSelect: "default"
        },
        comuna: {
            requiredSelect: "default"
        },
        vivienda: {
            requiredSelect: "default"
        },
    },
    messages: {
        correo: {
            required: "Debe ingresar un correo",
            email: "Ingrese un correo valido (ej: user@domain.ex)",
            maxlength: "Largo maximo 60"
        },
        run: {
            required: "Debe ingresar un Run",
            maxlength: "Largo maximo 10"
        },
        nombre: {
            required: "Debe ingresar un nombre",
            maxlength: "Largo maximo 100"
        },
        nacimiento: {
            required: "Debe ingresar una fecha de nacimiento",
            maxlength: "Largo maximo 10"
        },
        telefono: {
            number: "Debe ingresar un telefono valido",
            maxlength: "Largo maximo 9"
        },
        username: {
            required: "Debe ingresar un usuario",
            maxlength: "Largo maximo 40"
        },
        password: {
            required: "Debe ingresar una contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        repassword: {
            required: "Debe confirmar su contraseña",
            maxlength: "Largo maximo 50",
            equalTo: "Las contraseñas no son iguales"
        },
        pregunta: {
            requiredSelect: "Seleccionar una pregunta"
        },
        respuesta: {
            required: "Debe ingresar una respuesta",
            maxlength: "Largo maximo 60"
        },
        avatar: {
            required: "Debe seleccionar un avatar"
        },
        region: {
            requiredSelect: "Seleccione una region"
        },
        comuna: {
            requiredSelect: "Seleccione una comuna"
        },
        vivienda: {
            requiredSelect: "Seleccione una vivienda"
        }
    },
    submitHandler: function(form){
        agregar_usuario_social()
    }
})

//formulario de rescatados
$("#formulary-rescued").validate({
    rules: {
        nombrerescued: {
            required: true,
            maxlength: 60
        },
        raza :{
            required: true,
            maxlength: 40
        },
        foto: {
            required: true
        },
        descripcion: {
            required: true,
            maxlength: 200
        },
        estado: {
            requiredSelect: "default"
        }
    },
    messages: {
        nombrerescued: {
            required: "Debe ingresar un nombre",
            maxlength: "Largo maximo 60"
        },
        raza: {
            required: "Debe ingresar una raza",
            maxlength: "Largo maximo 40"
        },
        foto: {
            required: "Debe seleccionar una foto"
        },
        descripcion: {
            required: "Debe ingresar una descripcion",
            maxlength: "Largo maximo 200"
        },
        estado: {
            requiredSelect: "Seleccione un estado"
        }
    },
    submitHandler: function(form){
        action_rescued()
    }
})