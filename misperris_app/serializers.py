from rest_framework import serializers
from .models import Usuario, Rescatado

#django allauth
from allauth.socialaccount.models import SocialAccount

# Create your serializers here.
class UsuarioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Usuario
        fields = (
            'url',
            'id',
            'run',
            'nombre',
            'nacimiento',
            'telefono',
            'correo',
            'usuario',
            'password',
            'pregunta',
            'respuesta',
            'avatar',
            'region',
            'comuna',
            'vivienda',
            'provider'
        )

class RescatadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rescatado
        fields = (
            'url',
            'id',
            'nombre',
            'raza',
            'foto',
            'descripcion',
            'estado',
        )

class SocialSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SocialAccount
        fields = (
            'id',
            'provider',
            'uid',
            'last_login',
            'date_joined',
            'extra_data',
        )