from django.test import TestCase
from .models import Usuario, Rescatado


# Create your tests here.

class TestMisPerris(TestCase):
    def test_agregar_usuario(self):
        print("test agregar usuario")
        correo = 'test@test.cl'
        run = '11111111-1'
        nombre = 'test test test'
        nacimiento = '01/01/1990'
        telefono = 123456789
        username = 'test user'
        userpassword = 'test pass'
        avatar = 'avatar/default.png'
        pregunta = '¿test pregunta?'
        respuesta = 'test respuesta'
        region = 'test region'
        comuna = 'test comuna'
        vivienda = 'test vivienda'

        usuario = Usuario(correo = correo, run = run, nombre = nombre, nacimiento = nacimiento, telefono = telefono, usuario = username, userpassword = userpassword, avatar = avatar, pregunta = pregunta, respuesta = respuesta, region = region, comuna = comuna, vivienda = vivienda)

        try:
            usuario.save()
            res = True
        except:
            res = False
        
        return self.assertEqual(res, True)

    def test_agregar_rescatado(self):
        print("test agregar rescatado")
        nombre = 'test'
        raza = 'test raza'
        foto = 'avatar/default.png'
        descripcion = 'test descripcion'
        estado = 'test estado'

        rescatado = Rescatado(nombre = nombre, raza = raza, foto = foto, descripcion = descripcion, estado = estado)

        try:
            rescatado.save()
            res = True
        except:
            res = False

        return self.assertEqual(res, True)