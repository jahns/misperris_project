# Generated by Django 2.1.3 on 2018-11-23 02:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('misperris_app', '0002_auto_20181123_0221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='usuario',
            field=models.CharField(default='no-username', max_length=40),
        ),
    ]
