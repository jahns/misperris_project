from django.db import models

# Create your models here.
class Usuario(models.Model):
    run = models.CharField(max_length = 10)
    nombre = models.CharField(max_length = 100)
    nacimiento = models.CharField(max_length = 10)
    telefono = models.BigIntegerField()
    correo = models.CharField(max_length = 60)
    usuario = models.CharField(max_length = 40, unique = True, default = "no-username")
    password = models.CharField(max_length = 50, default = "no-password")
    pregunta = models.CharField(max_length = 60, default = "no-question")
    respuesta = models.CharField(max_length = 60, default = "no-answer")
    avatar = models.ImageField(upload_to = 'avatar/', default = 'avatar/default.png')
    region = models.CharField(max_length = 60)
    comuna = models.CharField(max_length = 60)
    vivienda = models.CharField(max_length = 60)
    provider = models.CharField(max_length = 20, default = "misperris")

    def __str__(self):
        return "usuario"

class Rescatado(models.Model):
    nombre = models.CharField(max_length = 60)
    raza = models.CharField(max_length = 40)
    foto = models.ImageField(upload_to = 'fotos/')
    descripcion = models.CharField(max_length = 200)
    estado = models.CharField(max_length = 50)

    def __str__(self):
        return "rescatados"
